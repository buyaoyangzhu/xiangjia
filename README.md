# xiangjia

#### 介绍

享+社区基本模板, 供学生使用

#### 使用说明

目的: 学生在老师最新提交的代码, 完成课堂案例, 保证随堂练习

1. 学生拉取代码到本地

2. 老师每完成一个案例, 会提交代码到 git

3. 学生在 vscode 中需要选择 **✨✨git bash✨✨** 的终端,注意是 git bash, 执行以下指令

```js
git fetch --all && git reset --hard origin/master && git pull && git reset --hard HEAD~1
```

#### 指令说明

这是一个组合命令, 分为三部分:

```js
git fetch --all: 拉取git仓库上最新的代码
git reset --hard origin/master: 服务器覆盖本地代码，以服务器代码为准，hard强制的意思，master要注意分支
git pull: 拉取最新的代码
git reset --hard HEAD~1: 再次回滚代码, 将代码回退到 N 个版本
```

#### 要求

1. 学生跟着老师完成随堂练习
2. 晚自习时再新建仓库，自己再来完成一遍

```js
git clone https://gitee.com/buyaoyangzhu/xiangjia -b template
```
