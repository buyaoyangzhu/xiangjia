// app.js
import './utils/utils'
import './utils/http'

App({
  /* 
    把 refreshToken 也存起来：用来token过期时刷新，实现无感刷新
  */
  token: '',
  refreshToken: '',
  getToken() {
    const token = wx.getStorageSync('token')
    this.token = token
    const refreshToken = wx.getStorageSync('refreshToken')
    this.refreshToken = refreshToken
  },
  setToken(token, refreshToken) {
    wx.setStorageSync('token', token)
    this.token = token
    wx.setStorageSync('refreshToken', refreshToken)
    this.refreshToken = refreshToken
  },
  onLaunch() {
    this.getToken()
  }
})
