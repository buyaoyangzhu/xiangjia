module.exports = (function() {
var __MODS__ = {};
var __DEFINE__ = function(modId, func, req) { var m = { exports: {}, _tempexports: {} }; __MODS__[modId] = { status: 0, func: func, req: req, m: m }; };
var __REQUIRE__ = function(modId, source) { if(!__MODS__[modId]) return require(source); if(!__MODS__[modId].status) { var m = __MODS__[modId].m; m._exports = m._tempexports; var desp = Object.getOwnPropertyDescriptor(m, "exports"); if (desp && desp.configurable) Object.defineProperty(m, "exports", { set: function (val) { if(typeof val === "object" && val !== m._exports) { m._exports.__proto__ = val.__proto__; Object.keys(val).forEach(function (k) { m._exports[k] = val[k]; }); } m._tempexports = val }, get: function () { return m._tempexports; } }); __MODS__[modId].status = 1; __MODS__[modId].func(__MODS__[modId].req, m, m.exports); } return __MODS__[modId].m.exports; };
var __REQUIRE_WILDCARD__ = function(obj) { if(obj && obj.__esModule) { return obj; } else { var newObj = {}; if(obj != null) { for(var k in obj) { if (Object.prototype.hasOwnProperty.call(obj, k)) newObj[k] = obj[k]; } } newObj.default = obj; return newObj; } };
var __REQUIRE_DEFAULT__ = function(obj) { return obj && obj.__esModule ? obj.default : obj; };
__DEFINE__(1686726990873, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _dot = _interopRequireDefault(require("@eivifj/dot"));

var _typecast = _interopRequireDefault(require("typecast"));

var _property = _interopRequireDefault(require("./property"));

var _messages = _interopRequireDefault(require("./messages"));

var _validators = _interopRequireDefault(require("./validators"));

var _error = _interopRequireDefault(require("./error"));

var _utils = require("./utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * A Schema defines the structure that objects should be validated against.
 *
 * @example
 * const post = new Schema({
 *   title: {
 *     type: String,
 *     required: true,
 *     length: { min: 1, max: 255 }
 *   },
 *   content: {
 *     type: String,
 *     required: true
 *   },
 *   published: {
 *     type: Date,
 *     required: true
 *   },
 *   keywords: [{ type: String }]
 * })
 *
 * @example
 * const author = new Schema({
 *   name: {
 *     type: String,
 *     required: true
 *   },
 *   email: {
 *     type: String,
 *     required: true
 *   },
 *   posts: [post]
 * })
 *
 * @param {Object} [obj] - schema definition
 * @param {Object} [opts] - options
 * @param {Boolean} [opts.typecast=false] - typecast values before validation
 * @param {Boolean} [opts.strip=true] - strip properties not defined in the schema
 * @param {Boolean} [opts.strict=false] - validation fails when object contains properties not defined in the schema
 */
var Schema = /*#__PURE__*/function () {
  function Schema() {
    var _this = this;

    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Schema);

    this.opts = opts;
    this.hooks = [];
    this.props = {};
    this.messages = Object.assign({}, _messages["default"]);
    this.validators = Object.assign({}, _validators["default"]);
    this.typecasters = Object.assign({}, _typecast["default"]);
    Object.keys(obj).forEach(function (k) {
      return _this.path(k, obj[k]);
    });
  }
  /**
   * Create or update `path` with given `rules`.
   *
   * @example
   * const schema = new Schema()
   * schema.path('name.first', { type: String })
   * schema.path('name.last').type(String).required()
   *
   * @param {String} path - full path using dot-notation
   * @param {Object|Array|String|Schema|Property} [rules] - rules to apply
   * @return {Property}
   */


  _createClass(Schema, [{
    key: "path",
    value: function path(_path, rules) {
      var _this2 = this;

      var parts = _path.split('.');

      var suffix = parts.pop();
      var prefix = parts.join('.'); // Make sure full path is created

      if (prefix) {
        this.path(prefix);
      } // Array index placeholder


      if (suffix === '$') {
        this.path(prefix).type(Array);
      } // Nested schema


      if (rules instanceof Schema) {
        rules.hook(function (k, v) {
          return _this2.path((0, _utils.join)(k, _path), v);
        });
        return this.path(_path, rules.props);
      } // Return early when given a `Property`


      if (rules instanceof _property["default"]) {
        this.props[_path] = rules; // Notify parents if mounted

        this.propagate(_path, rules);
        return rules;
      }

      var prop = this.props[_path] || new _property["default"](_path, this);
      this.props[_path] = prop; // Notify parents if mounted

      this.propagate(_path, prop); // No rules?

      if (!rules) return prop; // type shorthand
      // `{ name: String }`

      if (typeof rules == 'string' || typeof rules == 'function') {
        prop.type(rules);
        return prop;
      } // Allow arrays to be defined implicitly:
      // `{ keywords: [String] }`
      // `{ keyVal: [[String, Number]] }`


      if (Array.isArray(rules)) {
        prop.type(Array);

        if (rules.length === 1) {
          prop.each(rules[0]);
        } else {
          prop.elements(rules);
        }

        return prop;
      }

      var keys = Object.keys(rules);
      var nested = false; // Check for nested objects

      for (var _i = 0, _keys = keys; _i < _keys.length; _i++) {
        var key = _keys[_i];
        if (typeof prop[key] == 'function') continue;
        prop.type(Object);
        nested = true;
        break;
      }

      keys.forEach(function (key) {
        var rule = rules[key];

        if (nested) {
          return _this2.path((0, _utils.join)(key, _path), rule);
        }

        prop[key](rule);
      });
      return prop;
    }
    /**
     * Typecast given `obj`.
     *
     * @param {Object} obj - the object to typecast
     * @return {Schema}
     * @private
     */

  }, {
    key: "typecast",
    value: function typecast(obj) {
      var _loop = function _loop() {
        var _Object$entries$_i = _slicedToArray(_Object$entries[_i2], 2),
            path = _Object$entries$_i[0],
            prop = _Object$entries$_i[1];

        (0, _utils.enumerate)(path, obj, function (key, value) {
          if (value == null) return;
          var cast = prop.typecast(value);
          if (cast === value) return;

          _dot["default"].set(obj, key, cast);
        });
      };

      for (var _i2 = 0, _Object$entries = Object.entries(this.props); _i2 < _Object$entries.length; _i2++) {
        _loop();
      }

      return this;
    }
    /**
     * Strip all keys not defined in the schema
     *
     * @param {Object} obj - the object to strip
     * @param {String} [prefix]
     * @return {Schema}
     * @private
     */

  }, {
    key: "strip",
    value: function strip(obj) {
      var _this3 = this;

      (0, _utils.walk)(obj, function (path, prop) {
        if (_this3.props[prop]) return true;

        _dot["default"]["delete"](obj, path);

        return false;
      });
      return this;
    }
    /**
     * Create errors for all properties that are not defined in the schema
     *
     * @param {Object} obj - the object to check
     * @return {Schema}
     * @private
     */

  }, {
    key: "enforce",
    value: function enforce(obj) {
      var _this4 = this;

      var errors = [];
      (0, _utils.walk)(obj, function (path, prop) {
        if (_this4.props[prop]) return true;
        var error = new _error["default"](_messages["default"].illegal(path), path);
        errors.push(error);
        return false;
      });
      return errors;
    }
    /**
     * Validate given `obj`.
     *
     * @example
     * const schema = new Schema({ name: { required: true }})
     * const errors = schema.validate({})
     * assert(errors.length == 1)
     * assert(errors[0].message == 'name is required')
     * assert(errors[0].path == 'name')
     *
     * @param {Object} obj - the object to validate
     * @param {Object} [opts] - options, see [Schema](#schema-1)
     * @return {Array}
     */

  }, {
    key: "validate",
    value: function validate(obj) {
      var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      opts = Object.assign(this.opts, opts);
      var errors = [];

      if (opts.typecast) {
        this.typecast(obj);
      }

      if (opts.strict) {
        errors.push.apply(errors, _toConsumableArray(this.enforce(obj)));
      }

      if (opts.strip !== false) {
        this.strip(obj);
      }

      var _loop2 = function _loop2() {
        var _Object$entries2$_i = _slicedToArray(_Object$entries2[_i3], 2),
            path = _Object$entries2$_i[0],
            prop = _Object$entries2$_i[1];

        (0, _utils.enumerate)(path, obj, function (key, value) {
          var err = prop.validate(value, obj, key);
          if (err) errors.push(err);
        });
      };

      for (var _i3 = 0, _Object$entries2 = Object.entries(this.props); _i3 < _Object$entries2.length; _i3++) {
        _loop2();
      }

      return errors;
    }
    /**
     * Assert that given `obj` is valid.
     *
     * @example
     * const schema = new Schema({ name: String })
     * schema.assert({ name: 1 }) // Throws an error
     *
     * @param {Object} obj
     * @param {Object} [opts]
     */

  }, {
    key: "assert",
    value: function assert(obj, opts) {
      var _this$validate = this.validate(obj, opts),
          _this$validate2 = _slicedToArray(_this$validate, 1),
          err = _this$validate2[0];

      if (err) throw err;
    }
    /**
     * Override default error messages.
     *
     * @example
     * const hex = (val) => /^0x[0-9a-f]+$/.test(val)
     * schema.path('some.path').use({ hex })
     * schema.message('hex', path => `${path} must be hexadecimal`)
     *
     * @example
     * schema.message({ hex: path => `${path} must be hexadecimal` })
     *
     * @param {String|Object} name - name of the validator or an object with name-message pairs
     * @param {String|Function} [message] - the message or message generator to use
     * @return {Schema}
     */

  }, {
    key: "message",
    value: function message(name, _message) {
      (0, _utils.assign)(name, _message, this.messages);
      return this;
    }
    /**
     * Override default validators.
     *
     * @example
     * schema.validator('required', val => val != null)
     *
     * @example
     * schema.validator({ required: val => val != null })
     *
     * @param {String|Object} name - name of the validator or an object with name-function pairs
     * @param {Function} [fn] - the function to use
     * @return {Schema}
     */

  }, {
    key: "validator",
    value: function validator(name, fn) {
      (0, _utils.assign)(name, fn, this.validators);
      return this;
    }
    /**
     * Override default typecasters.
     *
     * @example
     * schema.typecaster('SomeClass', val => new SomeClass(val))
     *
     * @example
     * schema.typecaster({ SomeClass: val => new SomeClass(val) })
     *
     * @param {String|Object} name - name of the validator or an object with name-function pairs
     * @param {Function} [fn] - the function to use
     * @return {Schema}
     */

  }, {
    key: "typecaster",
    value: function typecaster(name, fn) {
      (0, _utils.assign)(name, fn, this.typecasters);
      return this;
    }
    /**
     * Accepts a function that is called whenever new props are added.
     *
     * @param {Function} fn - the function to call
     * @return {Schema}
     * @private
     */

  }, {
    key: "hook",
    value: function hook(fn) {
      this.hooks.push(fn);
      return this;
    }
    /**
     * Notify all subscribers that a property has been added.
     *
     * @param {String} path - the path of the property
     * @param {Property} prop - the new property
     * @return {Schema}
     * @private
     */

  }, {
    key: "propagate",
    value: function propagate(path, prop) {
      this.hooks.forEach(function (fn) {
        return fn(path, prop);
      });
      return this;
    }
  }]);

  return Schema;
}(); // Export ValidationError


exports["default"] = Schema;
Schema.ValidationError = _error["default"];
module.exports = exports.default;
}, function(modId) {var map = {"./property":1686726990874,"./messages":1686726990877,"./validators":1686726990878,"./error":1686726990875,"./utils":1686726990876}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1686726990874, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _error2 = _interopRequireDefault(require("./error"));

var _utils = require("./utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * A property instance gets returned whenever you call `schema.path()`.
 * Properties are also created internally when an object is passed to the Schema constructor.
 *
 * @param {String} name - the name of the property
 * @param {Schema} schema - parent schema
 */
var Property = /*#__PURE__*/function () {
  function Property(name, schema) {
    _classCallCheck(this, Property);

    this.name = name;
    this.registry = {};
    this._schema = schema;
    this._type = null;
    this.messages = {};
  }
  /**
   * Registers messages.
   *
   * @example
   * prop.message('something is wrong')
   * prop.message({ required: 'thing is required.' })
   *
   * @param {Object|String} messages
   * @return {Property}
   */


  _createClass(Property, [{
    key: "message",
    value: function message(messages) {
      if (typeof messages == 'string') {
        messages = {
          "default": messages
        };
      }

      var entries = Object.entries(messages);

      for (var _i = 0, _entries = entries; _i < _entries.length; _i++) {
        var _entries$_i = _slicedToArray(_entries[_i], 2),
            key = _entries$_i[0],
            val = _entries$_i[1];

        this.messages[key] = val;
      }

      return this;
    }
    /**
     * Mount given `schema` on current path.
     *
     * @example
     * const user = new Schema({ email: String })
     * prop.schema(user)
     *
     * @param {Schema} schema - the schema to mount
     * @return {Property}
     */

  }, {
    key: "schema",
    value: function schema(_schema) {
      this._schema.path(this.name, _schema);

      return this;
    }
    /**
     * Validate using named functions from the given object.
     * Error messages can be defined by providing an object with
     * named error messages/generators to `schema.message()`
     *
     * The message generator receives the value being validated,
     * the object it belongs to and any additional arguments.
     *
     * @example
     * const schema = new Schema()
     * const prop = schema.path('some.path')
     *
     * schema.message({
     *   binary: (path, ctx) => `${path} must be binary.`,
     *   bits: (path, ctx, bits) => `${path} must be ${bits}-bit`
     * })
     *
     * prop.use({
     *   binary: (val, ctx) => /^[01]+$/i.test(val),
     *   bits: [(val, ctx, bits) => val.length == bits, 32]
     * })
     *
     * @param {Object} fns - object with named validation functions to call
     * @return {Property}
     */

  }, {
    key: "use",
    value: function use(fns) {
      var _this = this;

      Object.keys(fns).forEach(function (name) {
        var arr = fns[name];
        if (!Array.isArray(arr)) arr = [arr];
        var fn = arr.shift();

        _this._register(name, arr, fn);
      });
      return this;
    }
    /**
     * Registers a validator that checks for presence.
     *
     * @example
     * prop.required()
     *
     * @param {Boolean} [bool] - `true` if required, `false` otherwise
     * @return {Property}
     */

  }, {
    key: "required",
    value: function required() {
      var bool = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      return this._register('required', [bool]);
    }
    /**
     * Registers a validator that checks if a value is of a given `type`
     *
     * @example
     * prop.type(String)
     *
     * @example
     * prop.type('string')
     *
     * @param {String|Function} type - type to check for
     * @return {Property}
     */

  }, {
    key: "type",
    value: function type(_type) {
      this._type = _type;
      return this._register('type', [_type]);
    }
    /**
     * Convenience method for setting type to `String`
     *
     * @example
     * prop.string()
     *
     * @return {Property}
     */

  }, {
    key: "string",
    value: function string() {
      return this.type(String);
    }
    /**
     * Convenience method for setting type to `Number`
     *
     * @example
     * prop.number()
     *
     * @return {Property}
     */

  }, {
    key: "number",
    value: function number() {
      return this.type(Number);
    }
    /**
     * Convenience method for setting type to `Array`
     *
     * @example
     * prop.array()
     *
     * @return {Property}
     */

  }, {
    key: "array",
    value: function array() {
      return this.type(Array);
    }
    /**
     * Convenience method for setting type to `Date`
     *
     * @example
     * prop.date()
     *
     * @return {Property}
     */

  }, {
    key: "date",
    value: function date() {
      return this.type(Date);
    }
    /**
     * Registers a validator that checks length.
     *
     * @example
     * prop.length({ min: 8, max: 255 })
     * prop.length(10)
     *
     * @param {Object|Number} rules - object with `.min` and `.max` properties or a number
     * @param {Number} rules.min - minimum length
     * @param {Number} rules.max - maximum length
     * @return {Property}
     */

  }, {
    key: "length",
    value: function length(rules) {
      return this._register('length', [rules]);
    }
    /**
     * Registers a validator that checks size.
     *
     * @example
     * prop.size({ min: 8, max: 255 })
     * prop.size(10)
     *
     * @param {Object|Number} rules - object with `.min` and `.max` properties or a number
     * @param {Number} rules.min - minimum size
     * @param {Number} rules.max - maximum size
     * @return {Property}
     */

  }, {
    key: "size",
    value: function size(rules) {
      return this._register('size', [rules]);
    }
    /**
     * Registers a validator for enums.
     *
     * @example
     * prop.enum(['cat', 'dog'])
     *
     * @param {Array} rules - allowed values
     * @return {Property}
     */

  }, {
    key: "enum",
    value: function _enum(enums) {
      return this._register('enum', [enums]);
    }
    /**
     * Registers a validator that checks if a value matches given `regexp`.
     *
     * @example
     * prop.match(/some\sregular\sexpression/)
     *
     * @param {RegExp} regexp - regular expression to match
     * @return {Property}
     */

  }, {
    key: "match",
    value: function match(regexp) {
      return this._register('match', [regexp]);
    }
    /**
     * Registers a validator that checks each value in an array against given `rules`.
     *
     * @example
     * prop.each({ type: String })
     * prop.each([{ type: Number }])
     * prop.each({ things: [{ type: String }]})
     * prop.each(schema)
     *
     * @param {Array|Object|Schema|Property} rules - rules to use
     * @return {Property}
     */

  }, {
    key: "each",
    value: function each(rules) {
      this._schema.path((0, _utils.join)('$', this.name), rules);

      return this;
    }
    /**
     * Registers paths for array elements on the parent schema, with given array of rules.
     *
     * @example
     * prop.elements([{ type: String }, { type: Number }])
     *
     * @param {Array} arr - array of rules to use
     * @return {Property}
     */

  }, {
    key: "elements",
    value: function elements(arr) {
      var _this2 = this;

      arr.forEach(function (rules, i) {
        _this2._schema.path((0, _utils.join)(i, _this2.name), rules);
      });
      return this;
    }
    /**
     * Registers all properties from the given object as nested properties
     *
     * @example
     * prop.properties({
     *   name: String,
     *   email: String
     * })
     *
     * @param {Object} props - properties with rules
     * @return {Property}
     */

  }, {
    key: "properties",
    value: function properties(props) {
      for (var _i2 = 0, _Object$entries = Object.entries(props); _i2 < _Object$entries.length; _i2++) {
        var _Object$entries$_i = _slicedToArray(_Object$entries[_i2], 2),
            prop = _Object$entries$_i[0],
            rule = _Object$entries$_i[1];

        this._schema.path((0, _utils.join)(prop, this.name), rule);
      }

      return this;
    }
    /**
     * Proxy method for schema path. Makes chaining properties together easier.
     *
     * @example
     * schema
     *   .path('name').type(String).required()
     *   .path('email').type(String).required()
     *
     */

  }, {
    key: "path",
    value: function path() {
      var _this$_schema;

      return (_this$_schema = this._schema).path.apply(_this$_schema, arguments);
    }
    /**
     * Typecast given `value`
     *
     * @example
     * prop.type(String)
     * prop.typecast(123) // => '123'
     *
     * @param {Mixed} value - value to typecast
     * @return {Mixed}
     */

  }, {
    key: "typecast",
    value: function typecast(value) {
      var schema = this._schema;
      var type = this._type;
      if (!type) return value;

      if (typeof type == 'function') {
        type = type.name;
      }

      var cast = schema.typecasters[type] || schema.typecasters[type.toLowerCase()];

      if (typeof cast != 'function') {
        throw new Error("Typecasting failed: No typecaster defined for ".concat(type, "."));
      }

      return cast(value);
    }
    /**
     * Validate given `value`
     *
     * @example
     * prop.type(Number)
     * assert(prop.validate(2) == null)
     * assert(prop.validate('hello world') instanceof Error)
     *
     * @param {Mixed} value - value to validate
     * @param {Object} ctx - the object containing the value
     * @param {String} [path] - path of the value being validated
     * @return {ValidationError}
     */

  }, {
    key: "validate",
    value: function validate(value, ctx) {
      var path = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this.name;
      var types = Object.keys(this.registry);

      for (var _i3 = 0, _types = types; _i3 < _types.length; _i3++) {
        var type = _types[_i3];

        var err = this._run(type, value, ctx, path);

        if (err) return err;
      }

      return null;
    }
    /**
     * Run validator of given `type`
     *
     * @param {String} type - type of validator
     * @param {Mixed} value - value to validate
     * @param {Object} ctx - the object containing the value
     * @param {String} path - path of the value being validated
     * @return {ValidationError}
     * @private
     */

  }, {
    key: "_run",
    value: function _run(type, value, ctx, path) {
      if (!this.registry[type]) return;
      var schema = this._schema;
      var _this$registry$type = this.registry[type],
          args = _this$registry$type.args,
          fn = _this$registry$type.fn;
      var validator = fn || schema.validators[type];
      var valid = validator.apply(void 0, [value, ctx].concat(_toConsumableArray(args), [path]));
      if (!valid) return this._error(type, ctx, args, path);
    }
    /**
     * Register validator
     *
     * @param {String} type - type of validator
     * @param {Array} args - argument to pass to validator
     * @param {Function} [fn] - custom validation function to call
     * @return {Property}
     * @private
     */

  }, {
    key: "_register",
    value: function _register(type, args, fn) {
      this.registry[type] = {
        args: args,
        fn: fn
      };
      return this;
    }
    /**
     * Create an error
     *
     * @param {String} type - type of validator
     * @param {Object} ctx - the object containing the value
     * @param {Array} args - arguments to pass
     * @param {String} path - path of the value being validated
     * @return {ValidationError}
     * @private
     */

  }, {
    key: "_error",
    value: function _error(type, ctx, args, path) {
      var schema = this._schema;
      var message = this.messages[type] || this.messages["default"] || schema.messages[type] || schema.messages["default"];

      if (typeof message == 'function') {
        message = message.apply(void 0, [path, ctx].concat(_toConsumableArray(args)));
      }

      return new _error2["default"](message, path);
    }
  }]);

  return Property;
}();

exports["default"] = Property;
module.exports = exports.default;
}, function(modId) { var map = {"./error":1686726990875,"./utils":1686726990876}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1686726990875, function(require, module, exports) {


function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

/**
 * Custom errors.
 *
 * @private
 */
var ValidationError = /*#__PURE__*/function (_Error) {
  _inherits(ValidationError, _Error);

  var _super = _createSuper(ValidationError);

  function ValidationError(message, path) {
    var _this;

    _classCallCheck(this, ValidationError);

    _this = _super.call(this, message);
    defineProp(_assertThisInitialized(_this), 'path', path);
    defineProp(_assertThisInitialized(_this), 'expose', true);
    defineProp(_assertThisInitialized(_this), 'status', 400);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(_assertThisInitialized(_this), ValidationError);
    }

    return _this;
  }

  return _createClass(ValidationError);
}( /*#__PURE__*/_wrapNativeSuper(Error));

exports["default"] = ValidationError;

var defineProp = function defineProp(obj, prop, val) {
  Object.defineProperty(obj, prop, {
    enumerable: false,
    configurable: true,
    writable: true,
    value: val
  });
};

module.exports = exports.default;
}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1686726990876, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.assign = assign;
exports.enumerate = enumerate;
exports.join = join;
exports.walk = walk;

var _dot = _interopRequireDefault(require("@eivifj/dot"));

var _componentType = _interopRequireDefault(require("component-type"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/**
 * Assign given key and value (or object) to given object
 *
 * @private
 */
function assign(key, val, obj) {
  if (typeof key == 'string') {
    obj[key] = val;
    return;
  }

  Object.keys(key).forEach(function (k) {
    return obj[k] = key[k];
  });
}
/**
 * Enumerate all permutations of `path`, replacing $ with array indices
 *
 * @private
 */


function enumerate(path, obj, callback) {
  var parts = path.split(/\.\$(?=\.|$)/);
  var first = parts.shift();

  var arr = _dot["default"].get(obj, first);

  if (!parts.length) {
    return callback(first, arr);
  }

  if (!Array.isArray(arr)) {
    return;
  }

  for (var i = 0; i < arr.length; i++) {
    var current = join(i, first);
    var next = current + parts.join('.$');
    enumerate(next, obj, callback);
  }
}
/**
 * Walk object and call `callback` with path and prop name
 *
 * @private
 */


function walk(obj, callback, path, prop) {
  var type = (0, _componentType["default"])(obj);

  if (type === 'array') {
    obj.forEach(function (v, i) {
      return walk(v, callback, join(i, path), join('$', prop));
    });
    return;
  }

  if (type !== 'object') {
    return;
  }

  for (var _i = 0, _Object$entries = Object.entries(obj); _i < _Object$entries.length; _i++) {
    var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
        key = _Object$entries$_i[0],
        val = _Object$entries$_i[1];

    var newPath = join(key, path);
    var newProp = join(key, prop);

    if (callback(newPath, newProp)) {
      walk(val, callback, newPath, newProp);
    }
  }
}
/**
 * Join `path` with `prefix`
 *
 * @private
 */


function join(path, prefix) {
  return prefix ? "".concat(prefix, ".").concat(path) : path;
}
}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1686726990877, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

/**
 * Default error messages.
 *
 * @private
 */
var Messages = {
  // Type message
  type: function type(prop, ctx, _type) {
    if (typeof _type == 'function') {
      _type = _type.name;
    }

    return "".concat(prop, " must be of type ").concat(_type, ".");
  },
  // Required message
  required: function required(prop) {
    return "".concat(prop, " is required.");
  },
  // Match message
  match: function match(prop, ctx, regexp) {
    return "".concat(prop, " must match ").concat(regexp, ".");
  },
  // Length message
  length: function length(prop, ctx, len) {
    if (typeof len == 'number') {
      return "".concat(prop, " must have a length of ").concat(len, ".");
    }

    var min = len.min,
        max = len.max;

    if (min && max) {
      return "".concat(prop, " must have a length between ").concat(min, " and ").concat(max, ".");
    }

    if (max) {
      return "".concat(prop, " must have a maximum length of ").concat(max, ".");
    }

    if (min) {
      return "".concat(prop, " must have a minimum length of ").concat(min, ".");
    }
  },
  // Size message
  size: function size(prop, ctx, _size) {
    if (typeof _size == 'number') {
      return "".concat(prop, " must have a size of ").concat(_size, ".");
    }

    var min = _size.min,
        max = _size.max;

    if (min !== undefined && max !== undefined) {
      return "".concat(prop, " must be between ").concat(min, " and ").concat(max, ".");
    }

    if (max !== undefined) {
      return "".concat(prop, " must be less than ").concat(max, ".");
    }

    if (min !== undefined) {
      return "".concat(prop, " must be greater than ").concat(min, ".");
    }
  },
  // Enum message
  "enum": function _enum(prop, ctx, enums) {
    var copy = enums.slice();
    var last = copy.pop();
    return "".concat(prop, " must be either ").concat(copy.join(', '), " or ").concat(last, ".");
  },
  // Illegal property
  illegal: function illegal(prop) {
    return "".concat(prop, " is not allowed.");
  },
  // Default message
  "default": function _default(prop) {
    return "Validation failed for ".concat(prop, ".");
  }
};
var _default2 = Messages;
exports["default"] = _default2;
module.exports = exports.default;
}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1686726990878, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _componentType = _interopRequireDefault(require("component-type"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * Default validators.
 *
 * @private
 */
var Validators = {
  /**
   * Validates presence.
   *
   * @param {Mixed} value - the value being validated
   * @param {Object} ctx - the object being validated
   * @param {Bolean} required
   * @return {Boolean}
   */
  required: function required(value, ctx, _required) {
    if (_required === false) return true;
    return value != null && value !== '';
  },

  /**
   * Validates type.
   *
   * @param {Mixed} value - the value being validated
   * @param {Object} ctx - the object being validated
   * @param {String|Function} name name of the type or a constructor
   * @return {Boolean}
   */
  type: function type(value, ctx, name) {
    if (value == null) return true;

    if (typeof name == 'function') {
      return value.constructor === name;
    }

    return (0, _componentType["default"])(value) === name;
  },

  /**
   * Validates length.
   *
   * @param {String} value the string being validated
   * @param {Object} ctx the object being validated
   * @param {Object|Number} rules object with .min and/or .max props or a number
   * @param {Number} [rules.min] - minimum length
   * @param {Number} [rules.max] - maximum length
   * @return {Boolean}
   */
  length: function length(value, ctx, len) {
    if (value == null) return true;

    if (typeof len == 'number') {
      return value.length === len;
    }

    var min = len.min,
        max = len.max;
    if (min && value.length < min) return false;
    if (max && value.length > max) return false;
    return true;
  },

  /**
   * Validates size.
   *
   * @param {Number} value the number being validated
   * @param {Object} ctx the object being validated
   * @param {Object|Number} size object with .min and/or .max props or a number
   * @param {String|Number} [size.min] - minimum size
   * @param {String|Number} [size.max] - maximum size
   * @return {Boolean}
   */
  size: function size(value, ctx, _size) {
    if (value == null) return true;

    if (typeof _size == 'number') {
      return value === _size;
    }

    var min = _size.min,
        max = _size.max;
    if (parseInt(min) != null && value < min) return false;
    if (parseInt(max) != null && value > max) return false;
    return true;
  },

  /**
   * Validates enums.
   *
   * @param {String} value the string being validated
   * @param {Object} ctx the object being validated
   * @param {Array} enums array with allowed values
   * @return {Boolean}
   */
  "enum": function _enum(value, ctx, enums) {
    if (value == null) return true;
    return enums.includes(value);
  },

  /**
   * Validates against given `regexp`.
   *
   * @param {String} value the string beign validated
   * @param {Object} ctx the object being validated
   * @param {RegExp} regexp the regexp to validate against
   * @return {Boolean}
   */
  match: function match(value, ctx, regexp) {
    if (value == null) return true;
    return regexp.test(value);
  }
};
var _default = Validators;
exports["default"] = _default;
module.exports = exports.default;
}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
return __REQUIRE__(1686726990873);
})()
//miniprogram-npm-outsideDeps=["@eivifj/dot","typecast","component-type"]
//# sourceMappingURL=index.js.map