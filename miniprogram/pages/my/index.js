Page({
  goLogin() {
    wx.navigateTo({
      url: '/pages/login/index',
    })
  },
  data: {
    userInfo: {}
  },
  /* 
  怎么选择生命周期
  1. 就在 onLoad 发送
  2. 假如你的页面是 列表/详情，一般涉及到增删改查的话，建议使用 onShow
  */
  onShow() {
    this.getUserInfo()
  },
  async getUserInfo() {
    const res = await wx.http({ url: '/userInfo' })
    this.setData({
      userInfo: res.data
    })
  }
})
