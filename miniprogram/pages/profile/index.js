// pages/profile/index.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: null
  },

  // 修改昵称
  async getUserNickName(e) {
    // console.log('e -----> ', e.detail.value);
    const nickName = e.detail.value
    await wx.http({ url: '/userInfo', method: 'PUT', data: { nickName } })
    this.setData({
      "userInfo.nickName": nickName
    })
    wx.utils.toast("修改昵称成功")
  },

  getUserAvatar(e) {
    // console.log('getUserAvatar -----> ', e.detail.avatarUrl);
    wx.uploadFile({
      // 后台地址，注意补全路径
      url: wx.http.baseURL + '/upload',
      // 文件资源
      filePath: e.detail.avatarUrl,
      // 一般写死file，后台小哥通过这个字段拿到数据
      name: 'file',
      // 请求头，注意 http 封装的只能在 wx.request使用，所以这里要再传递请求头
      header: {
        Authorization: `Bearer ${getApp().token}`
      },
      success: async (res) => {
        // console.log('uploadFile成功回调', res);
        /* 
          返回的数据再 res.data 中，是字符串，要 JSON.parse 转换
        */
        const result = JSON.parse(res.data)
        // console.log('result -----> ', result);
        const avatar = result.data.url
        this.setData({
          // 如果需要修改对象的某个属性，key可以使用字符串直接书写
          "userInfo.avatar": avatar
        })

        await wx.http({ url: '/userInfo', method: 'PUT', data: { avatar } })
        wx.utils.toast('头像修改成功')

      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad() {
    const res = await wx.http({ url: '/userInfo' })
    this.setData({
      userInfo: res.data
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})