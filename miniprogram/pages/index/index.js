/* 
1. 全局app引进，那么 wx 对象就有了 wx.utils 属性，任何页面都能使用了
2. 页面引进模块后，在调用 - 不建议
*/
// import utils from '../../utils/utils'
Page({
  data: {
    list: []
  },
  async onLoad() {
    // utils.toast('封装的方法')
    wx.utils.toast('wx全局调用')
    const res = await wx.http({
      url: '/announcement',
      header: { Authorization: 123, token: 123, haha: 222 }
    })
    // console.log('res -----> ', res);
    this.setData({
      list: res.data
    })
  }
})
