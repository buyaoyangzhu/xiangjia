import Schema from 'validate'

// 020-888888
const mobileSchema = new Schema({
  mobile: {
    type: String,
    required: true,
    match: /^1[3-9]\d{9}$/,
    message: '手机号码不正确~'
  },
})

// 登录校验，需要验证手机号和验证码
const loginSchema = new Schema({
  mobile: {
    type: String,
    required: true,
    match: /^1[3-9]\d{9}$/,
    message: '手机号码不正确~'
  },
  code: {
    type: String,
    required: true,
    length: { min: 6, max: 6 },
    message: '验证码不正确~'
  }

})


Page({
  data: {
    countDownVisible: false,
    mobile: '15915876390',
    code: '123456',
  },

  // 登录
  async login() {
    const data = { mobile: this.data.mobile, code: this.data.code }
    const errors = loginSchema.validate(data)
    if (errors.length) {
      wx.utils.toast(errors[0].message)
    } else {
      const res = await wx.http({ url: '/login', method: 'POST', data })
      const app = getApp()
      app.setToken(res.data.token, res.data.refreshToken)
      /* 
        登录后 - 回到来源页
        1. 获取页面访问记录 - getCurrentPages
        2. 通过传参数
        3. 把来源页存本地
        注意点：
        1. 加入直接用编译模式，在login页面执行，那么 报 currentPage 错，思路：可选链操作符
        2. 为什么不用 wx.navigateBack(),因为权限组件生命周期 attached 不会触发
      */
      const pages = getCurrentPages()
      const currentPage = pages[pages.length - 2].route
      console.log('pages -----> ', pages);
      console.log('currentPage -----> ', currentPage);
      wx.reLaunch({
        url: "/" + currentPage
      })
    }
  },

  // 发送验证码到后台
  sentCode() {
    const data = { mobile: this.data.mobile }
    const errors = mobileSchema.validate(data)
    if (!errors.length) {
      wx.http({ url: '/code', data })
      this.setData({
        countDownVisible: true
      })
    } else {
      wx.utils.toast(errors[0].message)
    }

  },

  countDownChange(ev) {
    console.log('ev -----> ', ev.detail);
    this.setData({
      timeData: ev.detail,
      // countDownVisible 为true,也就是有时间，就显示倒计时组件
      // 否则 显示 发送验证码
      countDownVisible: ev.detail.minutes > 0 || ev.detail.seconds > 0
    })
    // this.setData({
    //   timeData: ev.detail,
    //   countDownVisible: ev.detail.minutes === 1 || ev.detail.seconds > 0,
    // })
  },
})
