// pages/notify/index.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail: {}
  },

  /**
   * 生命周期函数--监听页面加载
   * 页面的参数，只能在 onLoad 中获取
   */
  async onLoad({ id }) {
    // console.log('option -----> ', id);
    const res = await wx.http({ url: `/announcement/${id}` })
    console.log('res -----> ', res);
    this.setData({ detail: res.data })
  },

})