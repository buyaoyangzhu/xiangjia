Page({
  data: {
    visitorList: []
  },
  async onLoad() {
    const res = await wx.http({ url: '/visitor' })
    this.setData({ visitorList: res.data.rows })
  },
  goPassport(e) {
    wx.navigateTo({
      url: '/visitor_pkg/pages/passport/index?id=' + e.mark.id,
    })
  },
})
