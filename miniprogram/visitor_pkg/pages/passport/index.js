Page({
  data: {
    id: "",
    houseInfo: "",
    url: "",
    validTime: 0,
    encryptedData: ""
  },
  async onLoad({ id, encryptedData }) {
    /* 
      业主进来，页面参数有id,发请求回来的数据有 encryptedData
      访客进来，页面参数没id，只有 encryptedData,返回数据没有 encryptedData
    */
    if (encryptedData) {
      //  访客
      const res = await wx.http({ url: `/visitor/share/${encryptedData}` })
      this.setData({ ...res.data })
    } else {
      const res = await wx.http({ url: `/visitor/${id}` })
      this.setData({
        ...res.data
      })
    }
  },
  // 倒计时结束事件
  onFinish() {
    this.setData({
      validTime: 0
    })
  },
  // 页面点击分享按钮，会触发这个固定的方法 onShareAppMessage
  onShareAppMessage() {
    return {
      title: '查看通行证',
      path: '/visitor_pkg/pages/passport/index?encryptedData=' + this.data.encryptedData,
      imageUrl: 'https://enjoy-plus.oss-cn-beijing.aliyuncs.com/images/share_poster.png',
    }
  },
  saveImg() {
    // 1. 调用downloadFile下载到本地,下载完存放到了缓存文件夹而已
    // 2. 把文件保存到相册
    wx.downloadFile({
      url: this.data.url,
      success: (res) => {
        // console.log('downloadFile -----> ', res.tempFilePath);
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success: () => {
            wx.utils.toast("保存到相册成功")
          }
        })
      }
    })
  }
})
