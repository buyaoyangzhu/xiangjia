// house_pkg/pages/building/index.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    point: '',
    // 要遍历的楼栋
    buildings: 0
  },

  getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad({ point }) {
    const buildings = this.getRandom(1, 10)
    this.setData({
      point,
      buildings
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})