/* 
resful 风格
get - 查询： 
  列表 /house
  详情 /house/id

put - 修改:
  /house, data:{data}

post - 新增：
  /house, data:{data}

delete - 删除:
  删除某一个 /house/id
*/

Page({
  editHouse() {
    wx.navigateTo({
      url: '/house_pkg/pages/form/index?id=' + this.data.id,
    })
  },
  // 其实数据不需要在data初始化，页面也能响应式变化
  // 但是还是建议，页面要用到的数据，都写在这里
  data: {
    id: '',
    // 小区名
    point: '',
    // 几号楼
    building: '',
    // 房间号
    room: '',
    // 业主姓名
    name: '',
    // 性别
    gender: '',
    // 手机号
    mobile: '',
    // 身份证正面
    idcardFrontUrl: '',
    // 身份证反面
    idcardBackUrl: '',
  },

  async onLoad({ id }) {
    console.log('id -----> ', id);
    const res = await wx.http({
      url: `/room/${id}`
    })
    console.log('res -----> ', res);
    this.setData({
      ...res.data
    })
    /* 
      把数据都存在 对象 obj 中，
      假如要做双向绑定，没办法使用 model:value
      建议把返回的数据的每一个属性，都放到 data 中会更好
    */

  }
})
