// house_pkg/pages/room/index.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    point: '',
    building: 1,
    rooms: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad({ point, building }) {
    let rooms = []
    for (let i = 0; i < this.getRandom(1, 10); i++) {
      const floor = this.getRandom(1, 32)
      const No = this.getRandom(1, 9)
      const room = `${floor}0${No}`
      // console.log('room -----> ', room);
      if (!rooms.includes(room)) {
        rooms.push(room)
      }
    }
    /* 
      房间号优化 - 数组排序，去重
    */
    rooms.sort((a, b) => a - b)


    this.setData({
      point, building, rooms
    })
  },

  getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})