

Page({
  data: {
    dialogVisible: false,
    houseList: []
  },

  async onLoad() {
    this.getHouseList()
  },
  async getHouseList() {
    const res = await wx.http({ url: '/room' })
    console.log('/room -----> ', res);
    this.setData({
      houseList: res.data
    })
  },

  swipeClose(ev) {
    /* 
      this.houseId 也可以赋值，但是页面不会有变化
      setData 方式，数据改变，页面也会响应式变化
    */
    console.log('ev -----> ', ev);
    this.houseId = ev.mark.id
    const { instance } = ev.detail
    instance.close()
    this.setData({
      dialogVisible: true
    })
  },

  async dialogClose(e) {
    // console.log('二次确认框 dialogClose -----> ', e);
    if (e.detail === "confirm") {
      await wx.http({ url: '/room/' + this.houseId, method: 'DELETE' })
      this.getHouseList()
    }
  },

  goDetail(ev) {
    wx.navigateTo({
      url: '/house_pkg/pages/detail/index?id=' + ev.mark.id,
    })
  },

  addHouse() {
    wx.navigateTo({
      url: '/house_pkg/pages/locate/index',
    })
  },
})
