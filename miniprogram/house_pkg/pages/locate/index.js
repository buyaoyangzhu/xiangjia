// house_pkg/pages/locate/index.ts
/* 
wx.getLocation()
wx.chooseLocation()
有了经纬度-- - 文字描述： 逆地址解析
文字描述 - 经纬度：正地址解析
keyword: 小区: poi检索
*/
import qqmap from "../../../utils/qqmap";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    address: '',
    // 小区数组集合
    points: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.getLocation()
  },
  async getLocation() {
    const res = await wx.getLocation()
    // console.log('经纬度 -----> ', res);
    const { latitude, longitude } = res
    qqmap.reverseGeocoder({
      location: {
        latitude,
        longitude
      },
      success: (res) => {
        console.log('地址逆解析', res);
        this.setData({
          address: res.result.address
        })
      }
    })
    this.search(latitude, longitude)
  },
  // 用户点击，自己选择位置 - 经纬度
  async onchooseLocation() {
    const res = await wx.chooseLocation()
    const { latitude, longitude } = res
    this.setData({
      address: res.address
    })
    this.search(latitude, longitude)
  },

  // poi检索: 根据地点搜索周边的 学校/小区/餐饮 等
  search(latitude, longitude) {
    qqmap.search({
      keyword: '住宅小区',
      location: {
        latitude,
        longitude
      },
      success: (res) => {
        // console.log('poi检索成功 -----> ', res.data);
        this.setData({
          /* 性能优化 - 实际中，其实比较少完成这种功能 */
          // points: res.data.map(v => {
          //   return { id: v.id, title: v.title }
          // })
          /* 
          代码优化 
          1. 箭头函数省略 return
          2. 函数参数的 解构
          */
          // points: res.data.map(v => ({ id: v.id, title: v.title }))
          points: res.data.map(({ id, title }) => ({ id, title }))
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})