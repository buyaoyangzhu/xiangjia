// \u4e00-\u9fa5] 中文验证规则
import Schema from 'validate'
/* 
因为 validate 校验数据时，
默认会把校验不通过的数据 - 删除，所以 id 会被删除掉，解决方案有2种
1. 在交验完后，把 id 再补回去
2. 修改插件的规则 - { strip: false }
*/
const formSchema = new Schema({
  point: {
    type: String,
    required: true,
    message: '小区不正确~'
  },
  building: {
    type: String,
    required: true,
    message: '楼栋不正确~'
  },
  room: {
    type: String,
    required: true,
    message: '房间号不正确~'
  },
  name: {
    type: String,
    required: true,
    message: '请输入业主姓名~'
  },
  gender: {
    type: Number,
    required: true,
    message: '请选择性别~'
  },
  mobile: {
    type: String,
    required: true,
    match: /^1[3-9]\d{9}$/,
    message: '手机号码不正确~'
  },

  idcardFrontUrl: {
    type: String,
    required: true,
    message: '请上传身份证正面照~'
  },
  idcardBackUrl: {
    type: String,
    required: true,
    message: '请上传身份证背面照~'
  },
})

Page({
  data: {
    name: 'ikun',
    gender: 1,
    mobile: '15915876390',
    idcardFrontUrl: 'http://yjy-teach-oss.oss-cn-beijing.aliyuncs.com/livimini/production/20230613/6908493346189312.jpg',
    idcardBackUrl: 'http://yjy-teach-oss.oss-cn-beijing.aliyuncs.com/livimini/production/20230613/6908493333688320.jpg',
    point: '泰然居',
    building: 2,
    room: '2023',
    id: ''
  },
  async onLoad({ point, building, room, id }) {
    if (id) {
      // 编辑 - 修改标题，发送详情请求
      wx.setNavigationBarTitle({ title: '编辑房屋信息' })
      const res = await wx.http({ url: '/room/' + id })
      this.setData({ ...res.data })
    } else {
      // 新增
      this.setData({
        point, building, room
      })
    }
  },
  /* 
    传递自定义属性区分正反面 - 两种写法
    1. 标签上添加 data-* 万能使用(原生，vue，react，uniapp，小程序都能用)，e.currentTarget.dataset.type
    2. ✅✅标签上添加 mark:* 小程序特有, e.mark.type
  */
  async onChooseMedia(e) {
    // console.log('data-* -----> ', e.currentTarget.dataset.type);
    console.log('e -----> ', e.mark.type);
    const res = await wx.chooseMedia({
      count: 1,
      mediaType: ['image']
    })
    console.log('onChooseMedia成功 -----> ', res.tempFiles[0].tempFilePath);
    wx.uploadFile({
      url: wx.http.baseURL + '/upload',
      filePath: res.tempFiles[0].tempFilePath,
      name: 'file',
      header: {
        Authorization: `Bearer ${getApp().token}`
      },
      success: (res) => {
        console.log('uploadFile -----> ', res);
        const result = JSON.parse(res.data)
        this.setData({
          [e.mark.type]: result.data.url
        })
      }
    })
  },
  async goList() {
    const data = { ...this.data }
    console.log('data -----> ', data);
    const errors = formSchema.validate(data)
    if (errors.length) {
      wx.utils.toast(errors[0].message)
      return
    }
    // 发请求
    console.log('发请求 -----> ');
    if (this.data.id) {
      data.id = this.data.id
      // 修改
      await wx.http({ url: '/room', method: 'POST', data })
      wx.utils.toast("修改成功")
    } else {
      // 新增
      await wx.http({ url: '/room', method: 'POST', data })
      wx.utils.toast("新增成功")
    }
    wx.navigateTo({ url: '/house_pkg/pages/list/index' })
  },

  removePicture(e) {
    console.log('e -----> ', e);
    this.setData({
      [e.mark.type]: ''
    })
  },
})
