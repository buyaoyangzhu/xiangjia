const utils = {
  toast(title) {
    wx.showToast({
      title,
      icon: 'none',
    })
  }
}
/* 
为了方便调用，直接放到 wx 全局对象上面，注意不要跟 原生api 重名
*/
wx.utils = utils

export default utils