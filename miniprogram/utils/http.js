// 导入 http 模块
import http from 'wechat-http'
/**
 * 配置接口基础路径
 */
http.baseURL = 'https://live-api.itheima.net'
/**
 * 挂载方法到全局
 */
wx.http = http

/**
 * 配置请求拦截器
 */
http.intercept.request = (config) => {
  console.log('config -----> ', config);
  const app = getApp()
  if (app.token) {
    config.header = {
      Authorization: `Bearer ${app.token}`,
      // 写在后，按用户传递过来的属性为准
      ...config.header
    }
  }
  return config
}
/**
 * 配置响应拦截器
 */
http.intercept.response = async (res) => {
  /* 
    假如返回的是 401，那么代表 token 过期，我们需要发送 refreshToken
  */
  if (res.statusCode === 401) {
    // 假如 /refreshToken 的请求都失败了，不要再发，请跳转登录页
    if (res.config.url.includes('/refreshToken')) {
      wx.navigateTo({ url: '/pages/login/index' })
      return
    }
    const app = getApp()
    const result = await wx.http({
      url: '/refreshToken', method: 'POST', header: {
        Authorization: `Bearer ${app.refreshToken}`
      }
    })
    console.log('无感刷新refreshToken -----> ', result);
    app.setToken(result.data.token, result.data.refreshToken)

    // console.log('token处理完成了 -----> ', res);
    // 要么跳转登录页，要么就把数据拿到
    // 请求接口可以在 res.config 拿到，注意里面的请求头还是失败的请求头，要修改为可以用的即可
    return wx.http({
      ...res.config,
      header: `Bearer ${result.data.token}`
    })
  }

  // 对返回码做判断，10000才是成功，否则要 Promise.reject(res)
  if (res.data.code === 10000) {
    return res.data
  } else {
    wx.utils.toast("请求失败")
    return Promise.reject(res)
  }
}
/**
 * 模块导出
 */
export default http
