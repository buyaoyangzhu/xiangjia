// components/authorization/authorization.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    isLogin: false
  },
  lifetimes: {
    attached(){
      const app = getApp()
      // console.log("app.token",app.token);
      this.setData({
        // !!一般用来快速把数据转成 布尔值
        isLogin: !!app.token
      })
      if(!app.token){
        wx.navigateTo({
          url: '/pages/login/index',
        })
      }
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
