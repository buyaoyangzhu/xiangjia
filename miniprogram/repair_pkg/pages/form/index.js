import dayjs from 'dayjs'
import Schema from 'validate'
const formSchema = new Schema({
  houseId: {
    type: String,
    required: true,
    message: '请选择维修的房屋'
  },
  repairItemId: {
    type: String,
    required: true,
    message: '请选择维修的项目'
  },
  mobile: {
    type: String,
    required: true,
    match: /^1[3-9]\d{9}$/,
    message: '请填写用户手机号'
  },
  appointment: {
    type: String,
    required: true,
    message: '请选择预约日期'
  },
  description: {
    type: String,
    required: true,
    length: { min: 1, max: 200 },
    message: '请输入问题描述,1-200字符'
  },
  attachment: {
    type: Array,
    required: true,
    length: { min: 0, max: 9 },
    each: {
      id: { type: String, required: true, message: '图片id必传' },
      url: { type: String, required: true, message: '图片url必传' },
    }
  }
})
/* 
  ### 扩展 - dayjs 介绍
  1. 解析： 一般用来获取 dayjs 的时间对象
  2. 取值/赋值(用的不多)： 一般取具体的时间，例如 dayjs().hour() -- 拿到当前的小时
  3. 操作： 对dayjs做时间的增加或减少
  4. 显示： 时间格式化,可以转换成 2022-01-01 或 时间戳 等时间格式
*/
Page({
  data: {
    // 房屋id
    houseId: '',
    // 房屋名称
    houseInfo: '',
    // 维修id
    repairItemId: '',
    // 维修项目
    repairItemName: '',
    // 预约的日期
    appointment: '',
    // 选中的时间戳
    currentDate: new Date().getTime(),
    // 最小预约日期
    minDate: +dayjs(),
    // 最大预约日期
    maxDate: +dayjs().add(3, 'day'),
    // 手机号码
    mobile: '15915876390',
    // 问题描叙
    description: '问题描叙',
    houseLayerVisible: false,
    repairLayerVisible: false,
    dateLayerVisible: false,
    houseList: [
      { name: '北京西三旗花园1号楼 101' },
      { name: '北京东村家园3号楼 302' },
      { name: '北京育新花园3号楼 703' },
      { name: '北京天通苑北苑8号楼 403' },
    ],
    repairItem: [],
    attachment: [],
  },
  async onLoad({ id }) {
    if (id) {
      // 编辑 - 修改标题，发详情请求
      wx.setNavigationBarTitle({ title: '修改报修信息' })
      const res = await wx.http({ url: `/repair/${id}` })
      this.setData({ ...res.data })
    } else {
      // 新增 - 暂无业务
    }
    // 房屋列表
    const res = await wx.http({ url: '/house' })
    this.setData({ houseList: res.data })

    // 维修项目列表
    const result = await wx.http({ url: '/repairItem' })
    this.setData({ repairItem: result.data })
  },
  // 图片上传回调
  afterRead(e) {
    console.log('e -----> ', e.detail.file.url);

    // 上传服务器的逻辑，需要自己去完成，vant组件只负责展示
    wx.uploadFile({
      url: wx.http.baseURL + '/upload',
      filePath: e.detail.file.url,
      name: 'file',
      header: {
        Authorization: `Bearer ${getApp().token}`
      },
      success: (res) => {
        const result = JSON.parse(res.data)
        console.log(' -----> ', result.data);
        this.setData({
          /* 
            1. 图片列表的数据是数组包含对象,对象中url是必须有的，应该 [{url:''},{url:''}]
            2. 每上传一张图片，直接覆盖了旧的数据，所以一值只有一张图片
          */
          // attachment: [result.data]
          // ✨✨建议写法,新建新数组，把旧的数据都先放进来，再添加新的数据
          attachment: [...this.data.attachment, result.data]
        })
      }
    })
  },
  // 图片删除
  onDelete(e) {
    // console.log('e -----> ', e.detail.index);
    const { index } = e.detail
    wx.showModal({
      title: '删除',
      content: `确定要删除第 ${index + 1} 张图片吗?`,
      success: (res) => {
        if (res.confirm) {
          const { attachment } = this.data
          attachment.splice(index, 1)
          // setData 页面响应式的更新
          this.setData({
            attachment
          })
        }
      }
    })
  },
  // 房屋选中事件
  onSelect(e) {
    console.log('房屋选中事件 -----> ', e.detail);
    this.setData({
      houseId: e.detail.id,
      houseInfo: e.detail.name
    })
  },
  // 维修项目选中
  onRepairSelect(e) {
    this.setData({
      repairItemId: e.detail.id,
      repairItemName: e.detail.name
    })
  },
  // 日期选中
  onSelectDate(e) {
    // console.log('日期选中 -----> ', e.detail);
    // console.log(' -----> ', dayjs(e.detail).format('YYYY-MM-DD'));
    this.setData({
      appointment: dayjs(e.detail).format('YYYY-MM-DD'),
      currentDate: e.detail,
      dateLayerVisible: false
    })
  },
  openHouseLayer() {
    this.setData({ houseLayerVisible: true })
  },
  closeHouseLayer() {
    this.setData({ houseLayerVisible: false })
  },
  openRepairLayer() {
    this.setData({ repairLayerVisible: true })
  },
  closeRepairLayer() {
    this.setData({
      repairLayerVisible: false,
    })
  },

  openDateLayer() {
    this.setData({ dateLayerVisible: true })
  },
  closeDateLayer() {
    this.setData({ dateLayerVisible: false })
  },
  async goList() {
    // 1. 准备后台要的数据
    const {
      houseId,
      repairItemId,
      mobile,
      appointment,
      description,
      attachment
    } = this.data

    const data = {
      houseId,
      repairItemId,
      mobile,
      appointment,
      description,
      attachment
    }
    // console.log('data -----> ', data);
    const errors = formSchema.validate(data)
    console.log('errors -----> ', errors);
    if (errors.length) {
      return wx.utils.toast(errors[0].message)
    }
    // 加入数据中有id，说明是修改，那么发送给后台的数据也要有id
    if (this.data.id) {
      data.id = this.data.id
    }

    // 数据没问题，提交数据，用户反馈，跳转页面
    await wx.http({ url: '/repair', method: 'POST', data })
    wx.showToast({ title: '成功' })
    setTimeout(() => {
      wx.navigateTo({
        url: '/repair_pkg/pages/list/index',
      })
    }, 1500);
  },
})
