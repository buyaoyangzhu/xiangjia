/* 
1. 点击跳转，带上id - mark:id
2. 根据id，发送请求，
3. 页面渲染
4. 地图 - 等会讲
*/

Page({
  data: {
    repairList: []
  },
  onLoad() {
    this.getRepair()
  },
  async getRepair() {
    const res = await wx.http({ url: '/repair' })
    this.setData({ repairList: res.data.rows })
  },
  goDetail(e) {
    wx.navigateTo({
      url: '/repair_pkg/pages/detail/index?id=' + e.mark.id,
    })
  },
  addRepair() {
    wx.navigateTo({
      url: '/repair_pkg/pages/form/index',
    })
  },
})
