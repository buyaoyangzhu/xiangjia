// map.js
import qqmap from "../../../utils/qqmap"
Page({
  data: {
    latitude: 23.127191,
    longitude: 113.355747,
    /* 
      markers 地图标记集合 - [{},{}] - 必传属性
      latitude,longitude 经纬度
      width,id 为了不报错，建议给上
      iconPath 可以不传
     */
    /* 
      polyline 路线数组 - [{},{}] - 必传属性
      points: [{经纬度},{经纬度},{经纬度}]
      width: 宽度
    */
    /* 
    注意点:
    1. 看不到效果，因为地图太大/太小了，标记的点不在范围内
    2. 经纬度不要写反，不要偏差太多，建议改小数点后2位即可
    */
    markers: [{
      latitude: 23.127191,
      longitude: 113.355747,
      // iconPath: '../../../static/images/marker.png',
      width: 40,
      height: 30,
      id: 1
    }, {
      latitude: 23.147191,
      longitude: 113.3555747,
      iconPath: '../../../static/images/marker.png',
      width: 40,
      height: 30,
      id: 2
    }],

    // 路线数组
    polyline: [
      {
        points: [{
          latitude: 23.127191,
          longitude: 113.355747,
        }, {
          latitude: 23.138191,
          longitude: 113.355847,
        }, {
          latitude: 23.137291,
          longitude: 113.365847,
        }, {
          latitude: 23.127191,
          longitude: 113.355747,
        },],
        width: 4,
        color: '#00b26a',
      }
    ],
    // 房屋id
    houseId: '',
    // 房屋名称
    houseInfo: '',
    // 维修id
    repairItemId: '',
    // 维修项目
    repairItemName: '',
    // 预约的日期
    appointment: '',
    // 手机号码
    mobile: '15915876390',
    // 问题描叙
    description: '问题描叙',
    // 问题附件
    attachment: [],
    // 维修状态
    status: 1,
  },
  async onLoad({ id }) {
    const res = await wx.http({ url: '/repair/' + id })
    this.setData({ ...res.data })
    this.geocoder()
    this.formSubmit()
  },
  // 路线规划
  formSubmit(e) {
    var _this = this;
    //调用距离计算接口
    qqmap.direction({
      mode: 'driving',//可选值：'driving'（驾车）、'walking'（步行）、'bicycling'（骑行），不填默认：'driving',可不填
      //from参数不填默认当前地址
      from: {
        latitude: 23.127191,
        longitude: 113.355747,
      },
      to: {
        latitude: 26.127191,
        longitude: 114.355747,
      },
      success: function (res) {
        console.log(res);
        var ret = res;
        var coors = ret.result.routes[0].polyline, pl = [];
        //坐标解压（返回的点串坐标，通过前向差分进行压缩）
        var kr = 1000000;
        for (var i = 2; i < coors.length; i++) {
          coors[i] = Number(coors[i - 2]) + Number(coors[i]) / kr;
        }
        //将解压后的坐标放入点串数组pl中
        for (var i = 0; i < coors.length; i += 2) {
          pl.push({ latitude: coors[i], longitude: coors[i + 1] })
        }
        console.log(pl)
        //设置polyline属性，将路线显示出来,将解压坐标第一个数据作为起点
        _this.setData({
          latitude: pl[0].latitude,
          longitude: pl[0].longitude,
          polyline: [{
            points: pl,
            color: '#FF0000DD',
            width: 4
          }]
        })
      },
      fail: function (error) {
        console.error(error);
      },
      complete: function (res) {
        console.log(res);
      }
    });
  },
  // 正地址解析
  geocoder() {
    qqmap.geocoder({
      address: '广州市天河区' + this.data.houseInfo,
      success: (res) => {
        // console.log('正地址解析 -----> ', res.result.location);
        const { lng, lat } = res.result.location
        this.setData({
          longitude: lng,
          latitude: lat
        })
      }
    })
  },
  // 取消报修
  test() {
    wx.showModal({
      title: '取消',
      content: "确定要取消该报修信息吗?",
      success: async ({ confirm }) => {
        if (confirm) {
          await wx.http({ url: '/cancel/repaire/' + this.data.id, method: 'PUT' })
          // 优化 - 用户提示，跳转列表
          wx.showToast({ title: '取消成功' })
          setTimeout(() => {
            wx.navigateTo({ url: '/repair_pkg/pages/list/index' })
          }, 1500);
        }
      }
    })
  },
  // 修改信息
  goForm() {
    wx.navigateTo({ url: '/repair_pkg/pages/form/index?id=' + this.data.id })
  }
})
